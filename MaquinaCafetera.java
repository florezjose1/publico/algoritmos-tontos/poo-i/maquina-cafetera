/**
 * Complete
 * @author Ejercicio: (Milton Jes�s Vera Contreras - miltonjesusvc@ufps.edu.co)
 * @author Desarrollador: (Jose Florez - florezjoserodolfo@gmail.com)
 * 
 * @version 0.000000000000001 :) --> Math.sin(Math.PI-Double.MIN_VALUE)
 */
class MaquinaCafetera { 
    int cafe;
    int azucar;
    int vasos;
    int precioBaseCafe;
    int egresos;
    int ingresos;
    int gananciasBrutas;
    int impuestos;
    int gananciasNetas;
    /*No requiere propiedades adicionales, pero es libre de usarlas*/

    MaquinaCafetera(){
        //complete
    }

    MaquinaCafetera(int cantidadInicialCafe, int cantidadInicialAzucar, int cantidadInicialVasos){
        //complete
        this.cafe = cantidadInicialCafe;
        this.azucar = cantidadInicialAzucar;
        this.vasos = cantidadInicialVasos;
    }
    
        //complete metodos GET / SET

    public void setCafe(int cafe) {
        this.cafe = cafe;
    }

    public int getCafe() {
        return this.cafe;
    }

    public void setAzucar(int azucar) {
        this.azucar = azucar;
    }

    public int getAzucar() {
        return this.azucar;
    }

    public void setVasos(int vasos) {
        this.vasos = vasos;
    }

    public int getVasos() {
        return this.vasos;
    }

    public void setPrecioBaseCafe(int precioBaseCafe) {
        this.precioBaseCafe = precioBaseCafe;
    }

    public int getPrecioBaseCafe() {
        return this.precioBaseCafe;
    }
    
    public void setEgresos(int egresos) {
        this.egresos = egresos;
    }
    
    public int getEgresos() {
        return egresos;
    }

    public void setIngresos(int ingresos) {
        this.ingresos = ingresos;
    }
    
    public int getIngresos() {
        return ingresos;
    }
    
    public void setImpuestos(int impuestos) {
        this.impuestos = impuestos;
    }

    public int getImpuestos() {
        return this.getIngresos() * 16 / 100;
    }

    public void setGananciasBrutas(int gananciasBrutas) {
        this.gananciasBrutas = gananciasBrutas;
    }

    public int getGananciasBrutas() {
        return this.getIngresos() - this.getEgresos();
    }

    public void setGananciasNetas(int gananciasNetas) {
        this.gananciasNetas = gananciasNetas;
    }
    
    public int getGananciasNetas() {
        return this.getGananciasBrutas() - this.getImpuestos();
    }


    public int calcularPrecio(int tipoCafe, int cantidadAzucar) {
        //complete
        int precio = this.getPrecioBaseCafe() / 1000 * 55;
        switch(tipoCafe){
            case 2:
                precio = precio * 2;
                break;
            case 3:
                precio = precio * 3;   
            default: break;
        }
        switch(cantidadAzucar){
            case 2:
                precio += precio * 5 / 100;
                break;
            case 3:
                precio += precio * 10 / 100;   
            default: break;
        }
        precio += precio * 15 / 100;
        return precio;
    }

    public boolean recargarCafe(int cantidadCafe, int costoCompraCafe) {
        //complete
        // Aca sucede la duda de cuando los valores deberian de ser calculados y almacenados, no se que tan bueno sea hacer todo un algoritmo para que
        // este recalculado los valores y luego los almacene o que simplemente los calcule a la hora de ser solicitados, para efectos del test, llamamos
        // directamente al atributo o propiedad de la clase porque el test 1 de recargar cafe hace un set a gananciasNetas, es decir, lo almacena, 
        // pero en si este deberia de ser calculado a partir de lo que entra y ha salido de la venta de cafe.
        if(this.getGananciasNetas() >= costoCompraCafe || this.gananciasNetas >= costoCompraCafe){
            this.cafe += cantidadCafe;
            this.registroFacturaEgreso(costoCompraCafe);
            return true;
        }
        return false;
    }
    
    public boolean recargarAzucar(int cantidadAzucar, int costoCompraAzucar) {
        //complete
        if(this.getGananciasNetas() >= costoCompraAzucar){
            this.azucar += cantidadAzucar;
            this.registroFacturaEgreso(costoCompraAzucar);
            return true;
        }
        return false;
    }

    public boolean recargarVasos(int cantidadVasos, int costoCompraVasos) {
        //complete
        if(this.getGananciasNetas()  >= costoCompraVasos){
            this.vasos += cantidadVasos;
            this.registroFacturaEgreso(costoCompraVasos);
            return true;
        }
        return false;
    }
    
     public void registroFacturaEgreso(int valorFactura) {
        this.egresos += valorFactura;
    }

    public boolean prepararCafe(int tipoCafe, int cantidadAzucar) {
        //complete
        boolean valorRetorno = false;
        if (this.getVasos() > 0){
            if (tipoCafe == 1 && this.getCafe() >= 55) {
                valorRetorno = this.restarCantidades(cantidadAzucar, 1);
            } else if (tipoCafe == 2 && this.getCafe() >= 55*2) {
                valorRetorno = this.restarCantidades(cantidadAzucar, 2);
            } else if (tipoCafe == 3 && this.getCafe() >= 55*3) {
                valorRetorno = this.restarCantidades(cantidadAzucar, 3);
            }
        }
        if(valorRetorno) {
            this.vasos--;
        }
        return valorRetorno;
    }

    public boolean restarCantidades(int cantidadAzucar, int tipo) {
        boolean valorRetorno = false;
        if (cantidadAzucar == 1) {
            valorRetorno = true;
        } else if(cantidadAzucar == 2 && this.getAzucar()>=5) {
            this.azucar -= 5;
            valorRetorno = true;
        } else if(cantidadAzucar == 3 && this.getAzucar()>=10) {
            this.azucar -= 10;
            valorRetorno = true;
        }
        if(valorRetorno) this.cafe -= 55 * tipo;
        return valorRetorno;
    }

    public void registrarFactura(int valorFactura) {
        this.ingresos += valorFactura;
    }
}
